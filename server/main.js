import { Meteor } from 'meteor/meteor';
import { Stu } from '../lib/collection/studentdb.js';


// Meteor.publish('stu', function () {
//   return Stu.find({});
// });
Meteor.publish('stu', function (searchText, skipCount, ) {  
    console.log('searchText' + searchText);
    return Stu.find({
        $or: [
          {
            txtfirstname: {'$regex' : '.*' +  searchText + '.*'}
          },
          {
            txtlastname: {'$regex' : '.*' + searchText + '.*'}
          },
          {
            txtage: searchText
          }
        ]
      },{limit: 2,skip:skipCount,sort:{txtdatein: -1}});
  });
//Typedocs Code
Meteor.methods({
  'createstu': function (pfname,plname,page) {
      Stu.insert({
          txtfirstname: pfname,
          txtlastname:plname,
          txtage: page
      }); 
  },
  'edittypedocs': function(typeID,pfname,plname,page){
        Stu.update(typeID,{$set:{txtfirstname:pfname,txtlastname:plname,txtage:page}
        });
    }
});