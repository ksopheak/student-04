import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import { Stu } from '../lib/collection/studentdb.js';


import './main.html';
import './w3.css';

Session.setDefault('skip', 0);
Session.setDefault('FilterText', '')
Tracker.autorun(() => {
    Meteor.subscribe('stu', Session.get('FilterText'), Session.get('skip'));
});


Template.inputdata.events({
    'click #signinbtn'(e) {
        e.preventDefault();
        var fname = document.getElementById("tfname").value;
        var lname = document.getElementById("tlname").value;
        var age = document.getElementById("tage").value;
        // alert(txtcode);
        Meteor.call("createstu",fname,lname,age);  
        // Clear form
        document.getElementById("tfname").value = '';
        document.getElementById("tlname").value = '';
        document.getElementById("tage").value = '';
        alert("Data Save!!!...");
        Session.set('FilterText', "");
    }
});

Template.datagrilview.events({
  'click #editme'(e){
        e.preventDefault();
        var postId = e.currentTarget.dataset.postId;
        var fname = document.getElementById("tfname").value;
        var lname = document.getElementById("tlname").value;
        var age = document.getElementById("tage").value;
        Meteor.call("edittypedocs",postId,fname,lname,age)
        document.getElementById("tfname").value = '';
        document.getElementById("tlname").value = '';
        document.getElementById("tage").value = '';
        alert("Data Update!!!...");
        Session.set('FilterText', "");
    },
    'click #previousData' (e){
        if(Session.get('skip') >= 2){
            Session.set('skip', Session.get('skip') - 2);
        }
    },
    'click #nextData' (e){
        if(Session.get('skip') <= 2){
            Session.set('skip', Session.get('skip') + 2);
        }
    }, 
    'keyup #sSearch' (e){
        var dInput = document.getElementById('sSearch').value;
        console.log(dInput);
        Session.set('FilterText', dInput);
    }
});

Template.datagrilview.helpers({
    stu(){    
        return Stu.find({});       
      }
});

